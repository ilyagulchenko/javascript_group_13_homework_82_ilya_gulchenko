import { createReducer, on } from '@ngrx/store';
import { clear, enter, number } from './keyboard.actions';

const correctPassword = '1337';
const initialState = '';
let newState = '';

export const counterReducer = createReducer(
  initialState,
  on(number, (state, props) => {
    if (state.length > 3) {
      return state;
    }
    newState += props.amount;
    console.log(newState);
    return state + '*';
  }),
  on(clear, (state) => {
    newState = newState.slice(0, -1);
    return state.slice(0, -1);
  }),
  on(enter, (state) => {
    if (newState === correctPassword) {
      alert('Access Granted');
      newState = '';
      return state = '';
    } else {
      alert('Access Denied');
      newState = '';
      return state = '';
    }
  })
);
