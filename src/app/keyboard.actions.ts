import { createAction, props } from '@ngrx/store';

export const number = createAction(
  '[Counter] Number',
  props<{amount: string}>()
);

export const clear = createAction('[Counter] Clear');

export const enter = createAction('[Counter] Enter');
