import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { clear, enter, number } from '../keyboard.actions';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.css']
})
export class KeyboardComponent {
  count!: Observable<string>;

  constructor(private store: Store<{count: string}>) {
    this.count = store.select('count');
  }

  seven() {
    this.store.dispatch(number({amount: '7'}));
  }

  eight() {
    this.store.dispatch(number({amount: '8'}));
  }

  nine() {
    this.store.dispatch(number({amount: '9'}));
  }

  four() {
    this.store.dispatch(number({amount: '4'}));
  }

  five() {
    this.store.dispatch(number({amount: '5'}));
  }

  six() {
    this.store.dispatch(number({amount: '6'}));
  }

  one() {
    this.store.dispatch(number({amount: '1'}));
  }

  two() {
    this.store.dispatch(number({amount: '2'}));
  }

  three() {
    this.store.dispatch(number({amount: '3'}));
  }

  zero() {
    this.store.dispatch(number({amount: '0'}));
  }

  clear() {
    this.store.dispatch(clear());
  }

  enter() {
    this.store.dispatch(enter());
  }

}
